;; -*- lexical-binding: t; -*-

;; Ivy settings
(use-package! ivy
  :bind
  ("M-n" . counsel-M-x)

  :custom
  ;; Show both index and count of matched items
  (ivy-count-format "%d/%d ")

  ;; Show bookmarks and recent files on disk in buffers list
  (ivy-use-virtual-buffers t)

  ;; Remove current & parent directories from counsel-find-file
  (ivy-extra-directories nil)

  ;; Use ivy-hydra for ivy-dispatching-done (M-o)
  ;; Ref: https://github.com/abo-abo/swiper/issues/2397
  (ivy-read-action-function 'ivy-hydra-read-action)

  :custom-face
  (ivy-current-match ((t (:weight bold :background "#0b0d0f"))))
  (ivy-minibuffer-match-face-1 ((t (:foreground "gray80" :background "#262830"))))
  (ivy-minibuffer-match-face-2 ((t (:foreground "orange red"))))
  (ivy-minibuffer-match-face-3 ((t (:foreground "yellow2"))))
  (ivy-minibuffer-match-face-4 ((t (:foreground "khaki"))))
  (ivy-subdir ((t (:foreground "gray50"))))
  (ivy-virtual ((t (:foreground "gray85"))))

  :config
  ;; Swap keybindings for ivy-done and ivy-alt-done
  ;; Use enter key to expand directories instead of opening in dired
  ;; Source: https://emacs.stackexchange.com/a/33706
  (with-eval-after-load 'counsel
    (let ((done (where-is-internal #'ivy-done ivy-minibuffer-map t))
          (alt (where-is-internal #'ivy-alt-done ivy-minibuffer-map t)))
      (define-key counsel-find-file-map done #'ivy-alt-done)
      (define-key counsel-find-file-map alt  #'ivy-done))))


(use-package! company
  :custom
  ;; Use numbers for quick completion
  (company-show-numbers t)

  ;; Cycle between suggestions
  (company-selection-wrap-around t)

  :config
  ;; Use tab key to cycle through suggestions ('tng' -> 'tab and go')
  (company-tng-mode 1))


;; Make company completions more information rich
(use-package! company-box
  :after
  (company)

  :hook
  (company-mode . company-box-mode))


;; Enable fuzzy completion
(use-package! company-flx
  :after
  (company)

  :config
  (company-flx-mode +1))


;; Make this file available as a package
(provide '+completion)
