;; -*- lexical-binding: t; -*-

;; Evil mode settings
(use-package! evil
  :after undo-tree

  :init
  ;; Suppress warning raised by evil-collection package
  (setq evil-want-keybinding nil)

  :custom
  ;; Use undo-tree for evil undo
  (evil-undo-system 'undo-tree)
  (evil-want-fine-undo t)

  :config
  (evil-mode 1)

  ;; Custom cursors for different evil modes
  (setq evil-emacs-state-cursor '(bar "white")
        evil-insert-state-cursor '(bar "white")
        evil-motion-state-cursor '(box "green")
        evil-normal-state-cursor '(box "white")
        evil-operator-state-cursor '(box "orange")
        evil-visual-state-cursor '(box "magenta")
        evil-replace-state-cursor '(box "brown"))

  ;; Replace insert state with emacs state
  (defalias 'evil-insert-state 'evil-emacs-state)

  ;; Use escape key to enter normal state from emacs state
  (evil-global-set-key 'emacs (kbd "<escape>") 'evil-normal-state)

  ;; Use menu key to toggle between emacs and normal state
  (evil-global-set-key 'emacs (kbd "M-i") 'evil-normal-state)
  (evil-global-set-key 'normal (kbd "M-i") 'evil-emacs-state)

  ;; Switch to normal state after save-buffer
  ;; NOTE: Adding this under :hook keyword breaks loading evil mode on startup
  ;; for some unkown reason!
  (add-hook 'before-save-hook #'evil-normal-state))


;; Make evil keybindings available in more places
(use-package! evil-collection
  :after evil

  :custom
  (evil-collection-setup-minibuffer t)

  :config
  (evil-collection-init)

  (evil-collection-define-key 'normal 'ivy-minibuffer-map
    "l" 'ivy-alt-done
    "gg" 'ivy-beginning-of-buffer
    "G" 'ivy-end-of-buffer)

  (evil-collection-define-key 'normal 'counsel-find-file-map
    "h" 'counsel-up-directory
    "l" 'ivy-alt-done))


;; Improved search and replace in evil mode
(use-package! evil-anzu
  :after evil)


;; Make this file available as a package
(provide 'init-evil)
