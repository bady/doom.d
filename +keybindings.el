;; -*- lexical-binding: t; -*-

;; Load bind-key package for easy binding
(require 'bind-key)

;; Common keybindings using M-SPC as some sort of leader key!
;; For functions/commands that are not part of any emacs/[m]elpa packages,
;; see 'init-functions.el
(bind-keys*
 :prefix-map meta-space-leader
 :prefix "M-SPC"
 ("M-SPC" . counsel-M-x)

 ;; Window management
 ("0" . delete-window)
 ("1" . toggle-maximize-buffer)
 ("2" . open-file-window-below)
 ("3" . open-file-window-right)

 ;; Buffer management
 ("h" . projectile-previous-project-buffer-maybe)
 ("l" . projectile-next-project-buffer-maybe)
 ("SPC" . save-buffer)
 ("k" . kill-this-buffer)
 ("b" . projectile-switch-to-buffer-maybe)

 ;; File management
 ("f" . counsel-find-file)
 ("j" . projectile-find-file-maybe)
 ("d" . projectile-find-file-dwim)
 ("w" . write-file)
 ("a" . fasd-find-file)
 ("i" . open-emacs-directory)
 ("o" . open-org-roam-directory)

 ;; Create new lines
 ("<return>" . evil-open-below)
 ("<S-return>" . evil-open-above)

 ;; Undo and redo
 ("u" . undo-tree-undo)
 ("r" . undo-tree-redo)

 ;; Toggle line numbering between absolute and relative
 ("t" . toggle-line-numbering)

 ;; Word correction using flyspell-correct-avy
 ("c" . flyspell-correct-wrapper)

 ;; Quickly search for org notes
 ("s" . deft)

 ;; Quickly jump to a visible line using avy
 ("g" . avy-goto-line)

 ;; Quickly jump to a word using avy
 ("v" . avy-goto-word-1)

 ;; Quickly jump to buffer poisitions indexed by imenu
 ("n" . counsel-imenu)

 ;; Insert org-mode links from clipboard
 ("y" . org-cliplink))


(bind-keys*
 ;; Switch windows
 ("M-h" . windmove-left)
 ("M-j" . windmove-down)
 ("M-k" . windmove-up)
 ("M-l" . windmove-right)

 ;; Manage recent files
 ("C-c C-f" . recentf-open-files)
 ("C-c C-e" . recentf-edit-list)

 ;; Comment line
 ("C-;" . comment-line)

 ;; Kill whole line
 ("C-'" . kill-whole-line)

 ;; Diff current buffer with file
 ("C-c d" . diff-buffer-with-file)

 ;; Revert buffer
 ("M-+" . revert-buffer)

 ;; Switch to minibuffer
 ("C-c C-m" . switch-to-minibuffer)

 ;; Toggle between beginning of line and beginning of text
 ("C-a" . smart-line-beginning)

 ;; Rename current buffer and associate file
 ("C-c r" . rename-this-buffer-and-file)

 ;; Toggle fontification
 ("M-*" . font-lock-mode))


(bind-keys
 ;; Verbose version of eval-buffer
 :map emacs-lisp-mode-map
 ("C-M-<return>" . eval-buffer-verbose))


;; Override doom bindings
(map! :after ivy
      :map general-override-mode-map
      :ei "M-SPC SPC" #'save-buffer)


;; Make this file available as a package
(provide 'init-keybindings)
