;; -*- lexical-binding: t; -*-

;; Prevent [m]elpa package files from showing in recent files list
;; Ref: https://www.reddit.com/r/emacs/comments/5wydsd/stop_recent_files_showing_elpa_packages/
(use-package! recentf
  :config
  (recentf-mode)
  (setq  recentf-max-saved-items 1000
         recentf-exclude '("^/var/folders\\.*"
                           "COMMIT_EDITMSG\\'"
                           ".*-autoloads\\.el\\'"
                           "[/\\]\\.elpa/")))


;; Smooth scrolling
(use-package! smooth-scrolling
  :config
  (smooth-scrolling-mode 1))


;; Preview line when executing goto-line command
(use-package! goto-line-preview
  :bind
  ([remap goto-line] . goto-line-preview))


;; Ranger-like dired file manager
(use-package! ranger
  :custom
  ;; Show hidden files and directories by default
  (ranger-show-hidden nil)

  ;; Don't hide cursor
  (ranger-hide-cursor nil)

  :config
  (ranger-override-dired-mode t))


;; Enable word-wrap (almost) everywhere
(+global-word-wrap-mode +1)

;; Don't spawn new buffers on entering each directory
(put 'dired-find-alternate-file 'disabled nil)


;; Make this file available as a pacakge
(provide '+misc)
