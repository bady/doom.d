;; -*- lexical-binding: t; -*-

;; Doom mode-line
(use-package! doom-modeline
  :custom
  ;; Show mini scrollbar
  (doom-modeline-hud t)

  :config
  ;; Set mode-line faces
  (set-face-background 'mode-line "#525658")
  (set-face-background 'mode-line-inactive "#161820"))


;; Highlight fill column (at column 80)
(use-package! display-fill-column-indicator
  :custom
  (display-fill-column-indicator-column 80)

  :config
  (global-display-fill-column-indicator-mode t)
  (set-face-attribute 'fill-column-indicator nil :foreground "#2B2D3F"))


;; Dim inactive buffers
(use-package! dimmer
  :custom
  ;; Make inactive buffer's text more dimmer (default is 0.2)
  (dimmer-fraction 0.4)

  :config
  ;; Exclude some buffers from dimming
  (dimmer-configure-which-key)
  (dimmer-configure-company-box)
  (dimmer-configure-magit)
  (dimmer-mode t))


;; Highlight parenthesis pairs
(show-paren-mode 1)

;; Hihglight text between parenthesis
(setq show-paren-style 'expression)

;; Show empty lines
(setq-default indicate-empty-lines t)

;; Make this file available as a package
(provide '+ui)
